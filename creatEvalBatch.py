import shutil
import os
import csv
from pathlib import Path

source_dir = "/home/bschembri/VisionAssignment/ArT_dataset/test_images/"
dest_dir = "/home/bschembri/VisionAssignment/ArT_dataset/"

x = range(1,8)
for i in x:
    with open("EvaluationFileSplit.csv", newline='', encoding='utf-8-sig') as csvfile:
        reader = csv.DictReader(csvfile)    
        dest = dest_dir + "test-images-" + str(i)
        Path(dest).mkdir(parents=True, exist_ok=True)
        for row in reader:
            if (int(row['batch']) == i):
                print("copying file: {}".format(row['file_name']))
                shutil.copyfile(f"{source_dir}/{row['file_name']}", f"{dest}/{row['file_name']}")        

    csvfile.close()
print("done")
