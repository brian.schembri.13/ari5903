# Brian Schembri
# brian.schembri.13@um.edu.mt
# As part of the submission for assignment ARI5903
# 27/6/2021

import gc
import torch

import argparse
import glob
import os
import time
import cv2
import json
import numpy as np

from detectron2.config import get_cfg


from predictor import VisualizationDemo

from shapely.geometry import Point
from shapely.geometry import Polygon

from ast import literal_eval


def setup_cfg(args):
    # load config from file and command-line arguments
    cfg = get_cfg()
    cfg.merge_from_file(args.config_file)
    cfg.merge_from_list(args.opts)
    # Set model
    cfg.MODEL.WEIGHTS = args.weights
    # Set score_threshold for builtin models
    cfg.MODEL.RETINANET.SCORE_THRESH_TEST = args.confidence_threshold
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = args.confidence_threshold
    cfg.MODEL.PANOPTIC_FPN.COMBINE.INSTANCES_CONFIDENCE_THRESH = args.confidence_threshold
    cfg.freeze()
    return cfg


def get_parser():
    parser = argparse.ArgumentParser(description="ARI5903 Assignment")
    parser.add_argument(
        "--config-file",
        default="./configs/ocr/totaltext_101_FPN.yaml",        
        metavar="FILE",
        help="path to config file",
    ) 
      # default="./configs/ocr/ctw1500_101_FPN.yaml",
      # default="./configs/ocr/synthtext_pretrain_101_FPN.yaml",      
      
    parser.add_argument(
        "--weights",        
        default="./out_dir_r101/totaltext_model/model_tt_r101.pth",        
        metavar="pth",
        help="the model used to inference",
    ) 
      # default="./out_dir_r101/ctw1500_model/model_ctw_r101.pth",
      # default="./out_dir_r101/syn_model/model_syn_r101_pretrain.pth",

    parser.add_argument(
        "--input",
        default="./input_images/*.*",
        #default='/home/bschembri/VisionAssignment/ArT_dataset/test_full_task2_images/*.*',
        nargs="+",
        help="the folder of totaltext test images"
    )

    parser.add_argument(
        "--output",
        default="./test_totaltext/",
        #default="./test_Eval/",
        help="A file or directory to save output visualizations. "
        "If not given, will show output in an OpenCV window.",
    )

    parser.add_argument(
        "--confidence-threshold",
        type=float,
        default=0.7,
        help="Minimum score for instance predictions to be shown",
    )
    parser.add_argument(
        "--opts",
        help="Modify config options using the command-line 'KEY VALUE' pairs",
        default=[],
        nargs=argparse.REMAINDER,
    )
    return parser


def compute_polygon_area(points):
    s = 0
    point_num = len(points)
    if(point_num < 3): return 0.0
    for i in range(point_num): 
        s += points[i][1] * (points[i-1][0] - points[(i+1)%point_num][0])
    return abs(s/2.0)
    

def get_polygons(prediction,polygons):

    classes = prediction['instances'].pred_classes
    polygons_list = []
    
    for i in range(len(classes)):
        if classes[i]==0:
            if len(polygons[i]) != 0:
                points = []
                for j in range(0,len(polygons[i][0]),2):
                    points.append([polygons[i][0][j],polygons[i][0][j+1]])
                points = np.array(points)
                area = compute_polygon_area(points)

                if area > 220:
                    tmp_list = []
                    tmp_list1 = []
                    tmp_list2 = []
                    tmp_list3 = []
                    for pt in polygons[i][0]:                        
                        tmp_list.append(pt)
                    tmp_list1 = [v for i, v in enumerate(tmp_list, start=0) if not i % 2]
                    tmp_list2 = [v for i, v in enumerate(tmp_list, start=0) if i % 2]
                    tmp_list3 = zip(tmp_list1, tmp_list2)                    
                    
                    polygons_list.append(literal_eval(str(list(zip(tmp_list1, tmp_list2))).strip('[]')))
                    
    return polygons_list

def save_result_to_txt(txt_save_path,prediction,polygons):

    file = open(txt_save_path,'w')
    classes = prediction['instances'].pred_classes

    for i in range(len(classes)):
        if classes[i]==0:
            if len(polygons[i]) != 0:
                points = []
                for j in range(0,len(polygons[i][0]),2):
                    points.append([polygons[i][0][j],polygons[i][0][j+1]])
                points = np.array(points)
                area = compute_polygon_area(points)

                if area > 220:
                    str_out = ''
                    for pt in polygons[i][0]:
                        str_out += str(pt)
                        str_out += ','
                    file.writelines(str_out+'###')
                    file.writelines('\r\n')
    file.close()

def is_pt_within_box(boundb, pt):

    # Note: boundb is of shape (4,)
    #       boundb[0] is bounding box top left x
    #       boundb[1] is bounding box top left y
    #       boundb[2] is the bottom right x
    #       boundn[3] is the bottom left y
    # and pt is the point we are checking for if it lies within
    # the boundb. pt is a list where pt[0] is the x and pt[1] is y 

    if pt[0] >= boundb[0] and pt[0] <= boundb[2] \
        and pt[1] >= boundb[1] and pt[1] <= boundb[3]:            
            
            return True
    else:
            return False


def is_within_box(boundb, innerb):

    # If top-left inner box corner is inside the bounding box
    if boundb[0] < innerb[0] and boundb[1] < innerb[1]:
        # If bottom-right inner box corner is inside the bounding box
        if innerb[2] < boundb[2] \
                and innerb[3] < boundb[3]:
            return True
        else:
            return False

def is_pont_in_polygon(polygon, box_centre_pt):
    region = Polygon([polygon]) # Shapely Polygon
    point = Point(box_centre_pt[0], box_centre_pt[1])
    
    if region.contains(point):
        return True
    else:
        return False
    
    
def get_word_indices(Boxes):    
    boxes = Boxes.tensor.cpu()
    boxes = boxes.numpy()
    text = [] # text-class bounding boxes
    text_orientation = []
    
    # Identify the text-class bounding boxes
    for i in range(len(boxes)):
        if prediction["instances"].pred_classes[i].item() == 0:
            # Check the reading orientation for each text-class box
            # If the x-axis is the longer we set x-oriented 
            # else we set y-oriented
            if (boxes[i][2] - boxes[i][0]) >= (boxes[i][3] - boxes[i][1]):
                text_orientation.append(tuple((i, 'x')))
            else:
                text_orientation.append(tuple((i, 'y')))
            text.append(i)

    words = []
    # Identify the words for the text bounding boxes
    for b in range(len(text)):
        for i in range(len(boxes)):
            if i not in text: # so if the box is not a text-class one
                # if is_within_box(boxes[b], boxes[i]):
                # Get the centre coordinat point of the box
                box_cntr = Boxes[i].get_centers()
                box_cntr = [box_cntr[0][0].item(), box_cntr[0][1].item()]

                # print('B-Box:', boxes[b], 'Box:', boxes[i], 'Box-Centre:', box_cntr)                
                if is_pt_within_box(boxes[b], box_cntr):
                    words.append(tuple((b,i)))
    
    return words, text_orientation
            
    
if __name__ == "__main__":
    
    eval_dict = {}
    
    args = get_parser().parse_args()

    cfg = setup_cfg(args)
    textFuseNet = VisualizationDemo(cfg)

    test_images_path = args.input
    output_path = args.output

    start_time_all = time.time()
    img_count = 0
    for i in glob.glob(test_images_path):
        #print("Processing File:", i)
        img_name = os.path.basename(i)
        img_fname = img_name.split('.')[0]
        img_fext = img_name.split('.')[1]
        img_save_path = output_path + img_fname + '.' + img_fext
        print("Image #: {} --> {}".format(img_count, img_fname))
        img = cv2.imread(i)
        start_time = time.time()
        
        prediction, vis_output, polygons = textFuseNet.run_on_image(img)
        # print("Classes:", prediction["instances"].pred_classes)
        words, text_orientation = get_word_indices(prediction["instances"].pred_boxes)
        if (len(prediction["instances"].pred_boxes.tensor.cpu().numpy()) > 0) and (len(words)>0):
            
            # Get word count
            w, cz= zip(*words)
            word_count = max(w)+1
            
            # Detectron2 meta data of indexes and classes
            meta_code = textFuseNet.metadata.thing_classes
            
            # Final list of decoded word of text
            word_txt = []
            
            # Order word-by-word        
            for i in range(word_count):
                # get Character id for each word being processed
                # from the tuple list named words
                c_id = [c[1] for c in words if i in c]             
                
                if (len(c_id) == 0):
                    break
                
                # Identify the orientation of the word - x or y based
                # Note: list with one item expected either ['x'] or ['y']
                word_orienation = [word[1] for word in text_orientation if word[0] == i]
    
                # In case a text-class bound box is blank - it happened
                # we set word_orienation to x
                if len(word_orienation) == 0:
                    word_orienation.append('x')
                
                tmp_word = []
                
                for j in c_id:
                    # get the respective x value of bounding box for the id
                    box_x = prediction["instances"].pred_boxes.tensor[j][0].item()
                    # add tuple of x value of bounding box and id
                    tmp_word.append(tuple((box_x, j)))
                
                # Order the word based on the respective axis
                if (word_orienation[0] == 'x'):            
                    tmp_word = sorted(tmp_word, key=lambda x: x[0])
                else:
                    tmp_word = sorted(tmp_word, key=lambda y: y[1])
                x, idx = zip(*tmp_word)
                
                word_str = ""
                for w_idx in idx:
                    class_id = prediction["instances"].pred_classes[w_idx].item()
                    c = meta_code[class_id]                
                    word_str = word_str + c
                
                # Print the extracted words from file
                print(word_str)
    
                word_txt.append(word_str)
            
            trans_tmp = ""
            for i in range(len(word_txt)):
                if i < (len(word_txt)-1): 
                    trans_tmp += word_txt[i] + ","
                else:
                    trans_tmp += word_txt[i]
                    
            eval_dict["res" + img_fname.split(".")[0].replace("gt", "")] = [{"transcription" : trans_tmp.strip()}]
        else:
            eval_dict["res" + img_fname.split(".")[0].replace("gt", "")] = [{"transcription" : ""}]
            
              
        #txt_save_path = output_path + 'res_img' + img_name.split('.')[0].split('img')[1] + '.txt'
        txt_save_path = output_path + 'res_img' + img_name.split('.')[0] + '.txt'
        save_result_to_txt(txt_save_path,prediction,polygons)
      
        vis_output.save(img_save_path)
        img_count += 1
        # print("Image #: {} --> {}".format(img_count, img_fname))
        
        gc.collect()

        torch.cuda.empty_cache()
    
    eval_file_list = args.output.split("/")
    eval_file = "" 
    for i in range(len(eval_file_list)-1):
        eval_file = eval_file + eval_file_list[i] +"/"
    eval_file = eval_file + "eval.json"
    with open(eval_file, 'w') as outfile:
        json.dump(eval_dict, outfile)
    outfile.close()
    print("JSON File created: {}".format(eval_file))
    
    print("Time: {:.2f} s / img".format(time.time() - start_time_all))
    print("Total Images: {}".format(img_count))
    print("Average Time: {:.2f} s /img".format((time.time() - start_time_all) / img_count))

export = False
if export:
    import pickle
    with open("box.pkl", 'wb') as f:
        boxes = prediction["instances"].pred_boxes.tensor.cpu()
        boxes = boxes.numpy()
        for i in range(0, len(boxes)):
            boxes[i] = boxes[i][0] 
            boxes[i] = boxes[i][1] 
            
            boxes[i] = boxes[i][2] 
            boxes[i] = boxes[i][3]
            
        pickle.dump(boxes, f)
