import os
import csv
import json

source_file = "/home/bschembri/VisionAssignment/ArT_dataset/test_full_other/files.csv"
eval_dict = {}

with open(source_file, newline='', encoding='utf-8-sig') as csvfile:
    reader = csv.DictReader(csvfile)    
    
    for row in reader:
        print("file: {}".format(row['file_name']))
        eval_dict["res" + row['file_name'].split(".")[0].replace("gt", "")] = [{"transcription" : ""}]

with open("eval_prob.json", 'w') as outfile:
        json.dump(eval_dict, outfile)
outfile.close()
csvfile.close()
print("done")
