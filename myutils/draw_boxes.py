#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  8 21:40:55 2021

@author: bschembri
"""

import pickle
from PIL import Image, ImageDraw

with open("../box.pkl", 'rb') as f:
    boxes = pickle.load(f)
    
with Image.open("../test_totaltext/CostaCoffee.jpg") as im:
    draw = ImageDraw.Draw(im)
    for box in boxes:
        box_coord = [box[0], box[1], box[2], box[3]]
        draw.rectangle(box_coord, fill="red", outline="blue", width=5)
        
    im.show()
    im.close()

